package apiTestingScript;


import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pom.Autoconst;

public class RestGetAPI implements Autoconst {
	
	RequestSpecification httpRequest;
	
	@BeforeMethod
	public void baseUrlInitialisation() {
		RestAssured.baseURI="https://reqres.in/";
		httpRequest= RestAssured.given();
	}
	
//	@Test
//	public void testCaseGetAPI(){
//		
////		From http://toolsqa.com/rest-assured/validate-response-status-code-using-rest-assured/
//
//		String value = "/api/users/2";
//		Response response = httpRequest.request(Method.GET, value);
//		
//		System.out.println("Statuscode" + response.getStatusCode());
//		System.out.println("Status Line is" + response.getStatusLine());
//		System.out.println("Response Body is =>  " +response.getBody().asString());
//		Assert.assertEquals(response.getStatusCode(),200 );
//		
//	}
	
//	public void testCaseGetAPI2() {
		
		/*//From http://artoftesting.com/automationTesting/restAPIAutomationGetRequest.html
		  //make get request to fetch capital of norway
				Response resp = get("http://restcountries.eu/rest/v1/name/norway");
				
				//Fetching response in JSON
				JSONArray jsonResponse = new JSONArray(resp.asString());
				
				//Fetching value of capital parameter
				String capital = jsonResponse.getJSONObject(0).getString("capital");
				
				//Asserting that capital of Norway is Oslo
				Assert.assertEquals(capital, "Oslo");*/
		
//	}
	
	@Test
	public void testCasePostAPI() {
		
		String value ="/api/users";
		Response response= httpRequest.request(Method.POST, value);
		System.out.println(response.getStatusCode());
		
		Assert.assertEquals(response.getStatusCode(),200 );

	}

	
	
}


