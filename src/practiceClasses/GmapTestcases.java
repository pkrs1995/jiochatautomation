package practiceClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import library.PageScreenshot;
import library.ReadExcelData;
import pom.Autoconst;

public class GmapTestcases {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeMethod
	void openMap() {
		System.setProperty(Autoconst.CHROME_KEY, Autoconst.CHROME_VALUE);
		driver =new ChromeDriver(); // Opening chrome browser
		driver.manage().window().maximize(); //Maximizing chrome browser
		wait =new WebDriverWait(driver,10);  //Created wait for 20second
		driver.get(Gmap.mapLink); //Opening google map
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmap.directionIcon))).click(); //clicking on direction button
		
	}
	
	@AfterMethod
	void closeMap() {
		driver.quit(); //closing the browser
		
	}
	
	@Test
	void testCases() {
		
		ReadExcelData red = new ReadExcelData(Gmap.xcelpath, 0);
		int totalTestCases = red.getLastRowNumber();
		System.out.println("Total "+totalTestCases+" going to check" );
		
		for(int i=1000;i<=totalTestCases;i++) {
			try {
				String from=red.getData(i,18)+" "+red.getData(i,19); //getting latlong
				String to = red.getData(i, 4)+","+red.getData(i, 6); // getting name with address
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmap.fromPlace))).sendKeys(from); //Entering latlong
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmap.toPlace))).sendKeys(to); // Entering address
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmap.toPlace))).sendKeys(Keys.ENTER); // Clicking Enter
				try {
					String distances = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmap.distanceInKm))).getText(); //Getting distance
					System.out.println("In Row "+i+ " Distance =" + distances); //Printing distance value
					red.setData(i, 22, distances); //Storing data in 22nd(w) column
					} catch (Exception e) {
						System.out.println("In Row "+i+ " Distance =" + "Something Went Wrong: " + e); //Printing distance value
						red.setData(i, 22, "SWW");
//					PageScreenshot.getScreenshot(driver, "GoogleMap");
				}
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmap.fromPlace))).clear(); //Clearing from field
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmap.toPlace))).clear(); //Clearing to field
			}catch (Exception e) {
				System.out.println("Refreshing the page because of error");
				driver.navigate().refresh();
				i--;
				
			}
			
		}
				
	}
	
	
}
