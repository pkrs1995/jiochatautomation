package practiceClasses;

public interface Gmap {
	
	String mapLink="https://www.google.com/maps";
	String directionIcon="//button[@id='searchbox-directions']";
	String fromPlace ="//div[@id='sb_ifc51']//input[@autocomplete='off']";
	String toPlace ="//div[@id='sb_ifc52']//input[@autocomplete='off']";
	String distanceInKm="//div[@class='section-directions-trip-distance section-directions-trip-secondary-text']";
	String xcelpath ="./dataProvider/ABG #1.xlsx";
	
}
