package library;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import pom.Autoconst;

public class Basetest implements Autoconst {
	
	public static WebDriver driver;
	
	@BeforeMethod
	@Parameters("browser")
	public void preconditions(String browserName) {
		
		try {
			if(browserName.equalsIgnoreCase("chrome")) {
				
				System.setProperty(CHROME_KEY, CHROME_VALUE);
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				driver =new ChromeDriver(options);
//			    driver.manage().window().maximize(); It's not working
				System.out.println("Chrome Browser Opened");
				
			}
			else if(browserName.equalsIgnoreCase("firefox") ||browserName.equalsIgnoreCase("mozila") ) {
				
				System.setProperty(GECO_KEY, GECO_VALUE);
				driver =new FirefoxDriver();
				driver.manage().window().maximize(); //It's working here
				System.out.println("Firefox Browser Opened");


			}
		} catch (Exception e) {
			System.out.println("Exception while choosing the browser:"+ e.getMessage() );
		}
		
		driver.manage().deleteAllCookies();
//		driver.get(JIO_URL);
		driver.navigate().to(JIO_URL);
		System.out.println("URL Entered in "+browserName);
		
	}
	
	@AfterMethod
	public void postconditions(ITestResult result) {
		if(ITestResult.FAILURE==result.getStatus()) {
			
			System.out.println("Screenshot Taken Because of Error");
			PageScreenshot.getScreenshot(driver, result.getName());
		
		}
		else 
			System.out.println(result.getName() +" Is Executed");
		
		driver.quit();
		System.out.println("Browser Closed");

	}

}
