package library;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Grid {
		
	public static WebDriver startNode( String nodeurl, String browser ){
		
		browser = browser.toLowerCase();
		DesiredCapabilities capabilities = null;
		switch (browser) {
		case "chrome"  :capabilities = DesiredCapabilities.chrome();
						capabilities.setBrowserName(browser);;
					 	break;
		case "firefox" :capabilities = DesiredCapabilities.firefox();
						capabilities.setBrowserName(browser);
					    break;
		case "ie"  	   :capabilities = DesiredCapabilities.internetExplorer();
						capabilities.setBrowserName(browser);
				 		break;
		default		   :System.out.println("Browser name incorrect");
						break;
		}
		capabilities.setPlatform(Platform.LINUX);
		WebDriver driver = new RemoteWebDriver(capabilities);
		driver.manage().deleteAllCookies();
		System.out.println(browser +"browser opened in" +nodeurl);
		return driver;
	}

}
