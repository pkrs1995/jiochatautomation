package library;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import pom.Autoconst;

public class GridBaseTest {
	
	public static WebDriver driver;
	
	@BeforeMethod
	@Parameters("browser")
	public void preconditions(String browserName) {
		driver =Grid.startNode(Autoconst.NODE1_URL, browserName);
		
	}
	
	@AfterMethod
	public void postconditions(ITestResult result) {
		driver.close();
	}
}
