package pom;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChatDetail {
	
	WebDriver driver;
	WebDriverWait wait;
//	Constructor
	public ChatDetail(WebDriver driver){
		this.driver= driver;
		PageFactory.initElements(driver, this);
		wait =new WebDriverWait(driver, 20);
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"app\"]/div[2]/div[1]/div/div/div/div[2]/div[2]/div[2]/div/div[1]/div[1]/div[1]/p")
	public WebElement CHATSNAME;
	public String getChatName() {
		return wait.until(ExpectedConditions.visibilityOf(CHATSNAME)).getText();
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"app\"]/div[2]/div/div/div/div/div[2]/div[2]/div[2]/div/div[3]/div/div[1]/div/div[2]/div[1]/textarea")
	public WebElement TEXTFIELD;
	public void sendTextToUser(String message) {
		wait.until(ExpectedConditions.visibilityOf(TEXTFIELD)).sendKeys(message);
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"app\"]/div[2]/div/div/div/div/div[2]/div[2]/div[2]/div/div[3]/div/div[1]/div/div[2]/div[2]/div[1]/div[1]/img")
	public WebElement ATTACHICON;
	public void clickAttachIcon() {
		wait.until(ExpectedConditions.visibilityOf(ATTACHICON)).click();
	}
	
	@FindBy(how=How.XPATH, using ="//*[@class=\"Menu__MenuOption\"]")
	public List <WebElement> ATTACHMENTOPTIONS;
	public void clickAttachmentOption(String option) {
		WebElement option0 = wait.until(ExpectedConditions.visibilityOf(ATTACHMENTOPTIONS.get(0)));
		WebElement option1 = wait.until(ExpectedConditions.visibilityOf(ATTACHMENTOPTIONS.get(1)));
		WebElement option2 = wait.until(ExpectedConditions.visibilityOf(ATTACHMENTOPTIONS.get(2)));
		WebElement option3 = wait.until(ExpectedConditions.visibilityOf(ATTACHMENTOPTIONS.get(3)));
		WebElement option4 = wait.until(ExpectedConditions.visibilityOf(ATTACHMENTOPTIONS.get(0)));
		WebElement option5 = wait.until(ExpectedConditions.visibilityOf(ATTACHMENTOPTIONS.get(1)));
		WebElement option6 = wait.until(ExpectedConditions.visibilityOf(ATTACHMENTOPTIONS.get(2)));
		WebElement option7 = wait.until(ExpectedConditions.visibilityOf(ATTACHMENTOPTIONS.get(3)));
		
		if(option.equalsIgnoreCase(option0.getText())) {
			option0.click();
		}
		else if(option.equalsIgnoreCase(option1.getText())) {
			option1.click();
		}
		else if(option.equalsIgnoreCase(option2.getText())) {
			option2.click();
		}
		else if(option.equalsIgnoreCase(option3.getText())) {
			option3.click();
		}
		else if(option.equalsIgnoreCase(option4.getText())) {
			option4.click();
		}
		else if(option.equalsIgnoreCase(option5.getText())) {
			option5.click();
		}
		else if(option.equalsIgnoreCase(option6.getText())) {
			option6.click();
		}
		else if(option.equalsIgnoreCase(option7.getText())) {
			option7.click();
		}
		else System.out.println("Wrong option clicked");

	}
	

}
