package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewGroupPopUp {
	
	WebDriver driver;
	WebDriverWait wait;
//	Constructor
	public NewGroupPopUp(WebDriver driver){
		this.driver= driver;
		PageFactory.initElements(driver, this);
		wait =new WebDriverWait(driver, 20);
	}
	
//	public By NEWGROUP_TITLE = By.cssSelector("#modal > div > p");
	@FindBy(how=How.CSS, using ="#modal > div > p")
	public WebElement NEWGROUP_TITLE ;
	public String getNewGroupPopUpTitle() {
		return wait.until(ExpectedConditions.visibilityOf(NEWGROUP_TITLE)).getText();	
	}
	
//	public By NEWGROUP_JIONUMBERS =By.xpath("//*[@id=\"JioContacts\"]");
	@FindBy(how=How.XPATH, using ="//*[@id=\"JioContacts\"]")
	public WebElement NEWGROUP_JIONUMBERS ;
	public void clickJioNumber() {
		wait.until(ExpectedConditions.visibilityOf(NEWGROUP_JIONUMBERS)).click();
	}
	
//	public By NEWGROUP_JIONUMBER_SEARCHFIELD = By.xpath("//*[@id=\"modal\"]/div/div/div[2]/div[2]/div/div/div[1]/input");
	@FindBy(how=How.XPATH, using ="//*[@id=\"modal\"]/div/div/div[2]/div[2]/div/div/div[1]/input")
	public WebElement NEWGROUP_JIONUMBER_SEARCHFIELD ;
	public void searchUser(String userName) {
		wait.until(ExpectedConditions.visibilityOf(NEWGROUP_JIONUMBER_SEARCHFIELD)).sendKeys(userName);
//		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
//	public By NEWGROUP_GROUPS_SEARCHFIELD = By.xpath("//*[@id=\"modal\"]/div/div/div[2]/div[2]/div[1]/input");
	@FindBy(how=How.XPATH, using ="//*[@id=\"modal\"]/div/div/div[2]/div[2]/div[1]/input")
	public WebElement NEWGROUP_GROUPS_SEARCHFIELD ;
	public void searchGroup(String groupName) {
		wait.until(ExpectedConditions.visibilityOf(NEWGROUP_GROUPS_SEARCHFIELD)).sendKeys(groupName);
	}
	
//	public By NEWGROUP_ADD1CONTACT = By.xpath("//*[@id=\"modal\"]/div/div/div[2]/div[2]/div/div/div[3]/div/img");
	@FindBy(how=How.XPATH, using ="//*[@id=\"modal\"]/div/div/div[2]/div[2]/div/div/div[3]/div/img")
	public WebElement NEWGROUP_1stCONTACT ;
	public void add1stContact() {
		wait.until(ExpectedConditions.visibilityOf(NEWGROUP_1stCONTACT)).click();
	}
	
//	public By NEWGROUP_ADDALLCONTACT = By.xpath("//*[@id=\"modal\"]/div/div/div[2]/div[2]/div/div/div[2]/img");
	@FindBy(how=How.XPATH, using ="//*[@id=\"modal\"]/div/div/div[2]/div[2]/div/div/div[2]/img")
	public WebElement NEWGROUP_SELECTALL;
	public void selectAllContacts() {
		wait.until(ExpectedConditions.visibilityOf(NEWGROUP_SELECTALL)).click();
	}
	
//	@FindBy(how=How.XPATH, using="//*[@id=\"modal\"]/div/div/div[2]/div[2]/div/div/div[3]/div[14]/img")
	
	
	@FindBy(how=How.XPATH, using="//*[@id=\"modal\"]/div/button")
	public WebElement CREATEGROUP_BUTTON;
	public void clickCreategroup() {
		wait.until(ExpectedConditions.visibilityOf(CREATEGROUP_BUTTON)).click();
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"modal\"]/div/div/div[2]/div/div[2]/div[2]/div/input")
	public WebElement GROUPNAMEFIELD;
	public void enterGroupName(String name) {
		wait.until(ExpectedConditions.visibilityOf(GROUPNAMEFIELD)).sendKeys(name);
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"modal\"]/div/div/div[2]/div/button")
	public WebElement CREATE_BUTTON;
	public void clickCreateButton() {
		wait.until(ExpectedConditions.visibilityOf(CREATE_BUTTON)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"modal\"]/img")
	public WebElement CLOSEICON;
	public void clickCloseIcon() {
		wait.until(ExpectedConditions.visibilityOf(CLOSEICON)).click();
	}
	
	public void crateNewGroup(WebDriver driver, String groupName) {
		ChatsTab chatstab = new ChatsTab(driver);
		chatstab.clickMoreOption();
		chatstab.clickFromOption("New Group");
		clickJioNumber();
		searchUser("Device");
		add1stContact();
		clickCreategroup();
		enterGroupName(groupName);
		clickCreateButton();
	}

	
	
}
