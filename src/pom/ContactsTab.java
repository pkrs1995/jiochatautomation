package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactsTab {
	
	WebDriver driver;
	WebDriverWait wait;
//	Constructor
	public ContactsTab(WebDriver driver){
		this.driver= driver;
		PageFactory.initElements(driver, this);
		wait =new WebDriverWait(driver, 20);
	}
	
	
	@FindBy(how=How.XPATH, using="//*[@id=\"id+917019473067\"]/div[3]/img[3]")
	public WebElement SENAPATI;
	public void clickSenapatiChatIcon() {
		wait.until(ExpectedConditions.visibilityOf(SENAPATI)).click();
	}

	
}
