package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Onboarding {
	
	WebDriver driver;
	WebDriverWait wait;
//	Constructor
	public Onboarding(WebDriver driver){
		this.driver= driver;
		PageFactory.initElements(driver, this);
		wait =new WebDriverWait(driver, 20);
	}
		
	@FindBy(how=How.XPATH, using ="//*[@id=\"phoneNumber\"]")
	public WebElement MOBILE_FIELD;
	public void insertMobileNumber(String number) {
		wait.until(ExpectedConditions.visibilityOf(MOBILE_FIELD)).sendKeys(number);
	}
	
	@FindBy(how=How.XPATH,using="//a[.=\"Terms and Conditions\"]")
	public WebElement TandC_LINK;
	public void clickTermandCondition() {
		wait.until(ExpectedConditions.visibilityOf(TandC_LINK)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[.=\"Accept and Continue\"]")
	public WebElement CONTINUE_BUTTON;
	public void clickContinueButton() {
		wait.until(ExpectedConditions.visibilityOf(CONTINUE_BUTTON)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[@class='clear country-sel-desktop']")
	public WebElement COUNTRY_SELECTION;
	public void clickCountry() {
		wait.until(ExpectedConditions.visibilityOf(COUNTRY_SELECTION)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"app\"]/div[2]/div/div/div/div[4]/div/div/input")
	public WebElement COUNTRY_SEARCHFIELD;
	public void searchForCountry(String countryName) {
		wait.until(ExpectedConditions.visibilityOf(COUNTRY_SEARCHFIELD)).sendKeys(countryName);
	}

	@FindBy(how=How.XPATH, using="//*[@id=\"app\"]/div[2]/div/div/div/div[4]/div/ul/li")
	public WebElement COUNTRY_SEARCHRESULT;
	public void selectCountry() {
		wait.until(ExpectedConditions.visibilityOf(COUNTRY_SEARCHRESULT)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"otpNumber\"]")
	public WebElement OTP_FIELD;
	public void insertOTP(String otp) {
		wait.until(ExpectedConditions.visibilityOf(OTP_FIELD)).sendKeys(otp);
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"app\"]/div[2]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div/div[4]/p")
	public WebElement RESEND_VERIFICATION_CODE;
	public void clickResendVerificationCode() {
		wait.until(ExpectedConditions.visibilityOf(RESEND_VERIFICATION_CODE)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[.=\"Done\"]")
	public WebElement DONE_BUTTON;
	public void clickDoneButton() {
		wait.until(ExpectedConditions.visibilityOf(DONE_BUTTON)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"profileName\"]")
	public WebElement NAME_FIELD;
	public void insertName(String name) {
		wait.until(ExpectedConditions.visibilityOf(NAME_FIELD)).sendKeys(name);
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"app\"]/div[2]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div/div[5]/button")
	public WebElement OK_BUTTON;
	public void clickOKButton() {
		wait.until(ExpectedConditions.visibilityOf(OK_BUTTON)).click();
	}
	
	@FindBy(how=How.CSS, using="#app > div.app-content > div > div > div > div.registration-container > div > div > div.inline-col-6.onboarding-left-card > div > div > div > div > div.container-row.clear.custom-phone-num > p")
	public WebElement ERROR_MOBNUM;
	public String getMobileNumberError() {
		return wait.until(ExpectedConditions.visibilityOf(ERROR_MOBNUM)).getText();
	}

	@FindBy(how=How.CSS, using="#app > div.app-content > div:nth-child(2) > div > div > div.registration-container > div > div > div.inline-col-6.onboarding-left-card > div > div > div > div > div.clear.custom-phone-num > p")
	public WebElement ERROR_OTPVERIFY;
	public String getOTPError() {
		return wait.until(ExpectedConditions.visibilityOf(ERROR_OTPVERIFY)).getText();
	}
	
	@FindBy(how=How.CSS, using="#app > div.app-content > div > div > div > div > div:nth-child(1) > header > div.header-left.header-img.display-flex.position-relative > p")
	public WebElement HOME_TEXT;
	public String getHomePageText() {
		return wait.until(ExpectedConditions.visibilityOf(HOME_TEXT)).getText();
	}
	
	public void doLginWith(String number, String otp) {
		
		insertMobileNumber(number);
		clickContinueButton();
		insertOTP(otp);
		clickDoneButton();
		
	}
	
	
}
