package pom;

public interface Autoconst {

	public String GECO_KEY = "webdriver.gecko.driver";

	public String GECO_VALUE = "./driver/geckodriver";

	public String CHROME_KEY = "webdriver.chrome.driver";

	public String CHROME_VALUE = "./driver/chromedriver";

	public String JIO_URL = "https://jio.divumapps.com";

	public String EXCEL_PATH = "./dataProvider/TestData.xlsx";
	
	public String HOMEPAGE_TEXT = "JioChat"+ System.lineSeparator() +"v1.0.16";
	
	public String MOBILEFIELD_ERROR = "Please enter your mobile number.";
	
	public String OTPFIELD_ERROR = "Please enter a valid 4 digit OTP";
	
	public String NODE1_URL ="";
	
	

}
