package pom;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ChatsTab {
	
	WebDriver driver;
	WebDriverWait wait;
//	Constructor
	public ChatsTab(WebDriver driver){
		this.driver= driver;
		PageFactory.initElements(driver, this);
		wait =new WebDriverWait(driver, 20);
	}
	
	
	@FindBy(how=How.XPATH, using= "//*[@id=\"app\"]/div[2]/div/div/div/div/div[1]/header/div[2]/div[1]/div[1]/img")
	public WebElement MORE_OPTION;
	public void clickMoreOption() {
		wait.until(ExpectedConditions.visibilityOf(MORE_OPTION)).click();
	}
	
	@FindBy(how=How.XPATH, using= "//*[@id=\"app\"]/div[2]/div/div/div/div/div[1]/header/div[2]/div[2]/img")
	public WebElement PROFILE_ICON;
	public void clickProfileIcon() {
		wait.until(ExpectedConditions.visibilityOf(PROFILE_ICON)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"react-menu-0\"]/div" )
	public List <WebElement> LISTOFOPTION;
	public void clickFromOption(String option) {
		
		WebElement option0 = wait.until(ExpectedConditions.visibilityOf(LISTOFOPTION.get(0)));
		WebElement option1 = wait.until(ExpectedConditions.visibilityOf(LISTOFOPTION.get(1)));
		WebElement option2 = wait.until(ExpectedConditions.visibilityOf(LISTOFOPTION.get(2)));
		WebElement option3 = wait.until(ExpectedConditions.visibilityOf(LISTOFOPTION.get(3)));
			
//		switch case is not working with string argument
		if(option.equalsIgnoreCase(option0.getText())) {
			option0.click();
		}
		else if(option.equalsIgnoreCase(option1.getText())) {
			option1.click();
		}
		else if(option.equalsIgnoreCase(option2.getText())) {
			option2.click();
		}
		else if(option.equalsIgnoreCase(option3.getText())) {
			option3.click();
		}
		else System.out.println("Wrong option clicked");
		
	}
	
	@FindBy(how=How.XPATH, using="" )
	public WebElement EXPLORETAB ;
	public void clickOnExploreTab() {
		wait.until(ExpectedConditions.visibilityOf(EXPLORETAB)).click();
	}
	
	@FindBy(how=How.XPATH, using="" )
	public WebElement CHANNELTAB ;
	public void clickOnChannelsTab() {
		wait.until(ExpectedConditions.visibilityOf(CHANNELTAB)).click();
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"app\"]/div[2]/div[1]/div/div/div/div[2]/div[1]/div[4]" )
	public WebElement CONTACTSTAB ;
	public void clickOnContactsTab() {
		wait.until(ExpectedConditions.visibilityOf(CONTACTSTAB)).click();
	}
	
	
	
	
	
}
