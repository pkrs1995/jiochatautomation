package testingScript;

import org.testng.annotations.Test;

import library.Basetest;
import library.verify;
import pom.ChatDetail;
import pom.ChatsTab;
import pom.NewGroupPopUp;
import pom.Onboarding;

public class CreateGroup extends Basetest {
	
	@Test
	public void createGroup_testCase1(){
		Onboarding onboarding = new Onboarding(driver);
		onboarding.doLginWith("8892446068", "4545");
		
		ChatsTab chatsTab = new ChatsTab(driver);
		chatsTab.clickMoreOption();
		chatsTab.clickFromOption("new group");
		
		NewGroupPopUp newgrouppopup =new NewGroupPopUp(driver);
		newgrouppopup.clickJioNumber();
		newgrouppopup.searchUser("Device");
		newgrouppopup.add1stContact();
		newgrouppopup.clickCreategroup();
		newgrouppopup.enterGroupName("Created by selenium");
		newgrouppopup.clickCreateButton();
		
		ChatDetail chatDetail = new ChatDetail(driver);
		verify.stringValues("Created by selenium", chatDetail.getChatName());
	}

}
