package testingScript;

import org.testng.Assert;
import org.testng.annotations.Test;

import library.Basetest;
import library.ReadExcelData;
import pom.Autoconst;
import pom.Onboarding;

public class Login extends Basetest {

	// ------------------------------------------------------------------------------------------
	@Test(description = "Login Positive testcase")
	public void login_testCase1() {
		
		ReadExcelData red = new ReadExcelData(Autoconst.EXCEL_PATH, 0);
		Onboarding onboarding =new Onboarding(driver);

		String mobileNumber = red.getData(0, 0);
		String otp = red.getData(0, 1);
		 
		onboarding.insertMobileNumber(mobileNumber);
		onboarding.clickContinueButton();
		onboarding.insertOTP(otp);
		onboarding.clickDoneButton();
		
		Assert.assertEquals(onboarding.getHomePageText(), Autoconst.HOMEPAGE_TEXT);

	}
//	 -------------------------------------------------------------------------------------------------
	 @Test(description ="Login Negative testcase:- Wrong Mobile number")
	 public void login_testCase2() {
		 
		Onboarding onboarding =new Onboarding(driver);
	
		onboarding.clickContinueButton();
		
		Assert.assertEquals(onboarding.getMobileNumberError(), Autoconst.MOBILEFIELD_ERROR);
	
	 }
//	 ---------------------------------------------------------------------------------------------------
	 @Test(description ="Login Negative testcase:- Wrong OTP ")
	 public void login_testCase3() {
		ReadExcelData red = new ReadExcelData(Autoconst.EXCEL_PATH, 0);
		Onboarding onboarding =new Onboarding(driver);
	
		String mobileNumber = red.getData( 0, 0);
	 	onboarding.insertMobileNumber(mobileNumber);
	 	onboarding.clickContinueButton();
	 	onboarding.clickDoneButton();

	 	Assert.assertEquals(onboarding.getOTPError(), Autoconst.OTPFIELD_ERROR);
	 
	 }

}
